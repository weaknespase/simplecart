/**
 * Cart api router, use with express application:
 *     app.use("/api", require("./routes/cart"))
 */

var express = require("express");
var bodyParser = require("body-parser");
var cartStore = require("../db/session-store");
var productStore = cartStore;
var sendObject = require("../handlers/utility-handlers").sendObject;

var sendError = function(res, errors) {
    res.status(400);
    sendObject(res, {
        error: {
            params: errors,
            type: "invalid_param_error",
            message: "Invalid data parameters"
        }
    });
};

var cart = express.Router();

cart.use(bodyParser.urlencoded({
    extended: false,
    type: "application/x-www-form-urlencoded"
}));

cart.get("/products", function(req, res, next) {
    productStore.productsGetList(function(err, dict) {
        if (err) {
            next(err);
        } else {
            var reply = { data: [] };
            for (var i in dict) {
                if (Object.hasOwnProperty.call(dict, i)) {
                    dict[i].id = i;
                    reply.data.push(dict[i]);
                }
            }
            res.status(200);
            sendObject(res, reply);
        }
    });
});

cart.route("/cart")
    .get(function(req, res, next) {
        cartStore.cartGetContents(req._session, function(err, cart) {
            if (err) {
                next(err);
            } else {
                productStore.productsGetList(Object.keys(cart), function(err, products) {
                    if (err) {
                        next(err);
                    } else {
                        var reply = {
                            data: {
                                total_sum: 0,
                                products_count: 0,
                                products: []
                            }
                        };
                        for (var i in cart) {
                            if (Object.hasOwnProperty.call(cart, i)) {
                                // Filter out invalid products in carts
                                if (products[i]) {
                                    var sum = products[i].price * cart[i];
                                    reply.data.products_count++;
                                    reply.data.total_sum += sum;
                                    reply.data.products.push({
                                        id: i,
                                        quantity: cart[i],
                                        sum: sum
                                    });
                                }    
                            }
                        }
                        res.status(200);
                        sendObject(res, reply);
                    }
                });
            }
        });
    })
    .post(function(req, res, next) {
        var errors = [];
        if (!req.body.product_id) errors.push({
            code: "required",
            message: "Product id can't be blank.",
            name: "product_id"
        });
        if (!req.body.quantity) errors.push({
            code: "required",
            message: "Quantity can't be blank.",
            name: "quantity"
        }); else if (!isFinite(req.body.quantity) || req.body.quantity <= 0)
            errors.push({
                code: "required",
                message: "Quantity must be greater than 0",
                name: "quantity"
            });
        if (errors.length > 0) {
            sendError(res, errors);
        } else {
            //Limit amount of data available to use as product_id
            req.body.product_id = req.body.product_id.substr(0, 64);
            productStore.productsGetList(req.body.product_id, function(err, dict) {
                if (err) { next(err); } else {
                    if (dict[req.body.product_id]) {
                        cartStore.cartPutItem(req._session, req.body.product_id, parseInt(req.body.quantity), function(err) {
                            if (err) next(err); else {
                                res.status(200);
                                res.send();
                            }
                        });
                    } else {
                        errors.push({
                            code: "required",
                            message: "Unknown product id specified.",
                            name: "product_id"
                        });
                        sendError(res, errors);
                    }
                }
            });
        }
    });

cart.delete("/cart", function(req, res, next) {
    cartStore.cartPurge(req._session, function(err, result) {
        if (err) next(err); else {
            res.status(200);
            res.send();
        }
    });
});

cart.delete("/cart/:product_id", function(req, res, next) {
    req.params.product_id = req.params.product_id.substr(0, 64);
    productStore.productsGetList(req.body.product_id, function(err, dict) {
        if (err) { next(err); } else {
            if (dict[req.params.product_id]) {
                cartStore.cartPutItem(req._session, req.params.product_id, -1, function(err) {
                    if (err) next(err); else {
                        res.status(200);
                        res.send();
                    }
                });
            } else {
                res.status(400);
                sendObject(res, {
                    error: {
                        "type": "invalid_param_error",
                        "message": "Unknown product id specified."
                    }
                });
            }
        }
    });
});

module.exports = cart;