/**
 * Example server making use of shopping cart module.
 */

var SESSION_COOKIE_NAME = "ssid";
var SESSION_COOKIE_AGE = 5 * 60;

var express = require("express");
var cookieParser = require("cookie-parser");
var sessionHandler = require("./handlers/session-handler");
var utilityHandlers = require("./handlers/utility-handlers");
var cart = require("./routes/cart");

var app = express();
app.use(cookieParser());
app.use(sessionHandler(SESSION_COOKIE_NAME, SESSION_COOKIE_AGE));
app.use("/api", cart);
app.use(utilityHandlers.illegalMethodHandler);
app.use(utilityHandlers.internalErrorHandler);
app.listen(4586, "127.0.0.1");

