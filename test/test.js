var assert = require("assert");

before(function(done) {
    // Initializing Redis database with test products
    require("./test-products").initTestDataset().then(done);
});

describe("http-api", function() {
    var http = require("http");
    var xcookie = "ssid=test-cookie-session-" + Date.now();
    function reader(cb) {
        return function(res) {
            var body = "";
            res.setEncoding("utf-8");
            res.on("data", function(chunk) {
                body += chunk;
            });
            res.on("end", function() {
                cb(body, res);
            });
        };
    }
    describe("/api/products", function() {
        it("should return correct response", function(done) {
            http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/products",
                headers: {
                    "Cookie": xcookie
                }
            }, reader(function(data) {
                var obj = JSON.parse(data);    
                try {
                    assert(obj.data, "Response without data.");
                    assert(Array.isArray(obj.data), "Products represented in non-array form.");
                    assert(obj.data.length > 0, "Missing products list for test completion.");
                    var bread = false, tank = false;
                    for (var i = 0; i < obj.data.length; i++){
                        bread |= obj.data[i].id == "bread";
                        tank |= obj.data[i].id == "tank";
                        assert(obj.data[i].name);
                        assert(obj.data[i].description);
                        assert(isFinite(obj.data[i].price));
                    }
                    assert(bread && !tank, "Data storage doesn't use test dataset.");
                    done();
                } catch (e) {
                    done(e);
                }    
            }))
                .on("error", done)
                .end();
        });
    });

    describe("/api/cart", function() {
        it("should add 3 bread", function(done) {
            var query = "product_id=bread&quantity=3";
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart",
                method: "POST",
                headers: {
                    "Cookie": xcookie,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Content-Length": query.length
                }
            }, function(res) {  
                res.resume();
                try {
                    assert.equal(res.statusCode, 200);
                    done();
                } catch (e) { done(e); }
            });
            req.on("error", done);
            req.write(query);
            req.end();
        });
        it("shouldn't add 1 tank", function(done) {
            var query = "product_id=tank&quantity=1";
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart",
                method: "POST",
                headers: {
                    "Cookie": xcookie,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Content-Length": query.length
                }
            }, function(res) {  
                res.resume();
                try {
                    assert.equal(res.statusCode, 400);
                    done();
                } catch (e) { done(e); }
            });
            req.on("error", done);
            req.write(query);
            req.end();
        });
        it("should error if adding negative quantity", function(done) {
            var query = "product_id=milk&quantity=-1";
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart",
                method: "POST",
                headers: {
                    "Cookie": xcookie,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Content-Length": query.length
                }
            }, reader(function(data, res) {
                try {
                    data = JSON.parse(data);
                    assert.equal(res.statusCode, 400);
                    assert(data.error);
                    assert(data.error.params.length == 1);
                    assert(data.error.params[0].name == "quantity");
                    done();
                } catch (e) { done(e); }
            }));
            req.on("error", done);
            req.write(query);
            req.end();
        });
        it("should report actual cart contents", function(done) {
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart",
                method: "GET",
                headers: {
                    "Cookie": xcookie
                }
            }, reader(function(data, res) {
                try {
                    data = JSON.parse(data);
                    assert.equal(res.statusCode, 200);
                    assert.deepEqual({
                        "data": {
                            "total_sum": 120,
                            "products_count": 1,
                            "products": [
                                {
                                    "id": "bread",
                                    "quantity": "3",
                                    "sum": 120
                                }
                            ]
                        }
                    }, data);
                    done();
                } catch (e) {
                    done(e);
                }
            }));
            req.on("error", done);
            req.end();
        });
        it("shouldn't be able to delete 1 tank from cart", function(done) {
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart/tank",
                method: "DELETE",
                headers: {
                    "Cookie": xcookie
                }
            }, reader(function(data, res) {
                try {
                    data = JSON.parse(data);
                    assert(res.statusCode, 400);
                    assert(data.error);
                    done();
                } catch (e) {
                    done(e);
                }
            }));
            req.on("error", done);
            req.end();
        });
        it("should report actual cart contents after deletion of 1 bread", function(done) {
            var req = http.request({
                host: "127.0.0.1",
                port: 4586,
                path: "/api/cart/bread",
                method: "DELETE",
                headers: {
                    "Cookie": xcookie
                }
            }, function(res) {
                res.resume();
                try {
                    assert.equal(res.statusCode, 200, "Removal must be successful");
                } catch (e) {
                    done(e);
                    return;
                }
                var req = http.request({
                    host: "127.0.0.1",
                    port: 4586,
                    path: "/api/cart",
                    method: "GET",
                    headers: {
                        "Cookie": xcookie
                    }
                }, reader(function(data, res) {
                    try {
                        data = JSON.parse(data);
                        assert.equal(res.statusCode, 200);
                        assert.deepEqual({
                            "data": {
                                "total_sum": 80,
                                "products_count": 1,
                                "products": [
                                    {
                                        "id": "bread",
                                        "quantity": "2",
                                        "sum": 80
                                    }
                                ]
                            }
                        }, data);
                        done();
                    } catch (e) {
                        done(e);
                    }
                }));
                req.on("error", done);
                req.end();
            });
            req.on("error", done);
            req.end();
        });
    });
});

describe("session-store", function() {
    var sstore = require("../db/session-store.js");
    describe("sessionGetNewKey()", function() {
        it("should return unique enough keys", function() {
            var key = sstore.sessionGetNewKey();
            assert.notEqual(key, sstore.sessionGetNewKey());
        });
    });

    var userSession = sstore.sessionGetNewKey();
    sstore.sessionTimeout = 5;
    describe("cartGetContents()", function() {
        it("should return empty object if cart doesn't exist yet", function(done) {
            sstore.cartGetContents(sstore.sessionGetNewKey(), function(err, cart) {
                if (err) done(err);
                try {
                    assert.deepEqual({}, cart);
                    done();
                } catch (e) {
                    done(e);
                }
            });
        });
        it("should return contents for cart, populated with cartPutItem()", function(done) {
            var cart = {
                "bread": 10,
                "milk": 4,
                "eggs": 31
            };
            var tasks = 0;
            function next() {
                if (--tasks == 0) {
                    sstore.cartGetContents(userSession, function(err, reply) {
                        if (err) done(err);
                        try {
                            assert.deepEqual(cart, reply);
                            done();
                        } catch (e) {
                            done(e);
                        }
                    });
                }
            }
            for (var i in cart) {
                if (Object.hasOwnProperty.call(cart, i)) {
                    tasks++;
                    sstore.cartPutItem(userSession, i, cart[i], next);
                }
            }
        });
        it("populated cart should expire after 5 seconds", function(done) {
            setTimeout(function() {
                sstore.cartGetContents(userSession, function(err, reply) {
                    if (err) done(err);
                    try {
                        assert.deepEqual({}, reply);
                        done();
                    } catch (e) {
                        done(e);
                    }
                });
            }, 5200);
        });
    });

    after(function() {
        sstore._shutdown();
    });
});