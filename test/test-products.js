/**
 * Populates Redis database with example products, also
 * serves as example of record structure.
 */

var redis = require("redis");

var client = redis.createClient(require("../db/redis.conf"));
var defineProduct = function(id, price, name, description) {
    return new Promise(function(resolve) {
        client.hset("products", id, JSON.stringify({
            name: name,
            description: description,
            price: price
        }), resolve);
    });
};

async function initTestDataset() {
    await defineProduct("bread", 40, "Bread", "A loaf of snow-white bread.");
    await defineProduct("milk", 95, "Cow milk 1l", "TetraPak of normalized cow milk, 3.5% fat.");
    await defineProduct("egg", 12, "Chicken egg", "Fresh chicken egg, C0.");
    await defineProduct("flour", 27, "Wheat flour", "Wheat flour, 1st category, 1 kg.");
    await defineProduct("salt", 4, "Salt", "500g of NaCl2 in plastic bag.");    
    client.quit();
}

module.exports.initTestDataset = initTestDataset;