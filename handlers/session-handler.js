/**
 * User session handler, for express application.
 *   app.use(require("./handlers/session-handler")(<session_cookie_name>,<session_timeout>))
 */

var sstore = require("../db/session-store");

module.exports = function(sessionCookieName = "ssid", sessionCookieAge = 300) {
    sstore.sessionTimeout = sessionCookieAge;
    return function(req, res, next) {
        var sid = req.cookies[sessionCookieName];
        if (!sid) {
            sid = sstore.sessionGetNewKey();
            res.setHeader("Set-Cookie", sessionCookieName + "=" + sid + "; Path=/");
        }
        req._session = sid;
        next();
    };
};