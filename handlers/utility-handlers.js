function sendObject(res, obj) {
    res.setHeader("Content-Type", "application/json");
    res.send(JSON.stringify(obj, null, 4));
}

module.exports.sendObject = sendObject;

module.exports.illegalMethodHandler = function(req, res, next) {
    res.status(404);
    sendObject(res, {
        "error": {
            "type": "invalid_request_error",
            "message": "Unable to resolve the request " + req.path + "."
        }
    });
};

module.exports.internalErrorHandler = function(err, req, res, next) {
    console.error(err);
    if (res.headersSent) {
        return next();
    }
    res.status(500);
    res.send("500 Internal Server Error");
};

