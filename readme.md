Пример простой корзины покупок для реализации магазина.

## Подробности

Для хранения сессий пользователя, корзин продуктов, которые они создают, и описания типов продуктов используется Redis. Описания продуктов хранятся в формате сериализованного JSON в хэш-таблице `products`. Схема одной записи:

    {
        "name": "название продукта",
        "description": "описание продукта",
        "price:" "цена 1 шт."
    }

Добавлять и удалять записи продуктов можно без остановки сервера.

Корзины пользователей хранятся с использование идентификатора сессии в хэш-таблицах `session:<key>:cart`.

## Тестирование

Проект включает в себя несколько тестов, написанных для фреймворка тестирования `mocha`. Для запуска:

    redis-server redis.conf &
    node index.js &
    sleep 2
    mocha --timeout 999999

## Установка

Весь основной функционал представлен в модулях `handlers/session-handler.js`, `db/session-store.js` и `routes/cart.js`. В `index.js` есть пример монтирования маршрутизатора корзины в `/api`. Для установки в готовый проект написанный с использованием Express, необходимо: 
1. Копировать вместе с содержимым `db` и `routes`.
2. Указать актуальную конфигурацию для клиента Redis в `db/redis.conf.js` (см. документацию к node-redis).
3. Убедиться, что механизм пользовательских сессий создает свойство `_session` у объекта запроса перед передачей его машрутизатору корзины, или использовать обработчик из `handlers/session-handler.js`.
4. Подключить корзину к приложению, модуль экспортирует интерфейс в виде Express `Router`.