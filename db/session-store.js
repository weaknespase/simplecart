/**
 * Session storage module. Redis backend.
 * Also doubles as products description storage.
 */

var crypto = require("crypto");
var redis = require("redis");
var log = console.log.bind(null, "session-store " + process.pid + ":");
var errlog = console.error.bind(null, "session-store " + process.pid + ":");

// Session-related parameters
var sessionTimeout = 5 * 60;
var sessionCounter = (Date.now() % 1e9) * ((process.pid & 0xffff) + 1);
var sessionKeyword = "Session-on-Redis-857544";

// Use proper configuration of redis to conect
var client = redis.createClient(require("./redis.conf.js"));

/**
 * Generates unique session key.
 */
var genSessionKey = function() {
    var hash = crypto.createHash("sha256");
    hash.update(++sessionCounter + sessionKeyword);
    return hash.digest().toString("base64").replace(/=/g, "");
};

var getCartKey = function(key) {
    return "session:" + key + ":cart";
};

// Client initialization
client.on("error", function(err) {
    if (err instanceof redis.RedisError) {
        //Redis error handling
        if (err instanceof redis.AbortError){
            errlog(err);
        } else if (err instanceof redis.AggregateError) {
            errlog("Redis errors -", err.errors.length, err);
        }
    } else {
        //System error handling
        if (err.code == "ECONNREFUSED") {
            errlog("Redis server is not accessible");
        } else {
            errlog("System error", err);
        }
    }
});
client.on("ready", function() {
    log("Redis connection successful");
});

Object.defineProperty(module.exports, "sessionTimeout", {
    get: function() { return sessionTimeout; },
    set: function(value) {
        if (isFinite(value) && value > 0) {
            sessionTimeout = value;
        }
    }
});

/**
 * Retrieves cart contents using session key
 * @param {string} key session key
 * @param {function(Error, Object<string,number>)} cb callback
 */
module.exports.cartGetContents = function(key, cb) {
    client.hgetall(getCartKey(key), function(err, reply) {
        if (err)
            cb(err);
        else if (reply == null)
            cb(null, {});
        else cb(null, reply);
        // Errors handled internally
        client.expire(getCartKey(key), sessionTimeout);
    });
};

/**
 * Modifies cart contents by adding or removing specified amount of product.
 * @param {string} key session key
 * @param {string|number} item item id
 * @param {number} amount amount to add (use negative to remove)
 * @param {function(Error)} cb callback
 */
module.exports.cartPutItem = function(key, item, amount, cb) {
    client.hincrby(getCartKey(key), item, amount, function(err, reply) {
        if (err) {
            cb(err);
        } else {
            if (reply <= 0) {
                //Remove item from cart
                client.hdel(getCartKey(key), item, cb());
            } else {
                cb(null);
            }
            // Errors handled internally
            client.expire(getCartKey(key), sessionTimeout);
        }
    });
};

/**
 * Clears contents of cart, returns true if cart contained any items.
 * @param {string} key session key
 * @param {function(Error, Boolean)} cb callback
 */
module.exports.cartPurge = function(key, cb) {
    client.del(getCartKey(key), function(err, reply){
        if (err) {
            cb(err);
        } else {
            cb(null, reply);
        }
    });
};

/**
 * Retrieves descriptions of selected products.
 * @param {string|string[]} [list] list of specific product ids to retrieve, retrieves all if missing
 * @param {function(Error,Object<string,{name:string,description:string,price:number}>)} cb callback
 */
module.exports.productsGetList = function(list, cb) {
    if (typeof list == "string") list = [list];
    if (list instanceof Function) {
        cb = list;
        list = null;
    }
    if (list) {
        if (list.length > 0)
            client.hmget("products", list, process);
        else {
            cb(null, {});
        }
    } else {
        client.hgetall("products", process);
    }

    function process(err, products) {
        if (err) {
            cb(err);
        } else {
            if (Array.isArray(products)) {
                var obj = {};
                products.forEach(function(v, i) {
                    obj[list[i]] = v;
                });
                products = obj;
            }
            try {
                for (var i in products) {
                    if (Object.hasOwnProperty.call(products, i))
                        if (products[i])
                            products[i] = JSON.parse(products[i]);
                }
                cb(null, products);
            } catch (e) {
                cb(e);
            }
        }
    }
};

/**
 * Returns new unique session key.
 */
module.exports.sessionGetNewKey = function() {
    return genSessionKey();
};

module.exports._shutdown = function() {
    client.quit();
};